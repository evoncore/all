export default function({ mongoose, connection }, { uuid, unix }) {
  const schema = new mongoose.Schema({
    id: {
      type: String,
      required: true,
      default: uuid,
    },
    profileId: {
      type: String,
      required: true,
    },
    dialogId: {
      type: String,
      required: true,
    },
    body: {
      type: String,
      required: true,
    },
    readed: {
      type: Boolean,
      default: false,
    },
    createdAt: {
      type: Number,
      required: true,
      default: unix,
    },
    updatedAt: {
      type: Number,
      required: true,
      default: unix,
    },
  });

  return connection.model('Message', schema);
}