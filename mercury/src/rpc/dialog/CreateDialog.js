import { parallel } from 'async';
import { isEmpty } from 'lodash';

export default ({ Dialog }, { createWaterfall }, { Caesar }) => function({request}, response) {
  const waterfall = createWaterfall();
  const {users} = request;
  const [user1, user2] = users;

  // validate request
  waterfall.step('1', function(callback) {
    if (user1 === user2) {
      return callback(new Error('You can’t create dialog with yourself.'));
    }

    callback();
  });

  // find existing dialog for this users pair
  waterfall.step('2', function(callback) {
    Dialog.find({ users: { $in: users } }, function(err, dialog) {
      if (err) return callback(err);

      if (!isEmpty(dialog)) {
        return callback(new Error('Dialog for this users pair already exists.'));
      }

      callback();
    });
  });

  // check for existing users
  waterfall.step('3', function(callback) {
    parallel([
      function(callback) {
        Caesar.getOne({ id: user1 }, function(error, payload) {
          if (error) {
            return callback(new Error(error.details));
          } else if (isEmpty(payload)) {
            return callback(new Error('Cannot get user for create dialog.'));
          }
      
          callback();
        });
      },

      function(callback) {
        Caesar.getOne({ id: user2 }, function(error, payload) {
          if (error) {
            return callback(new Error(error.details));
          } else if (isEmpty(payload)) {
            return callback(new Error('Cannot get user for create dialog.'));
          }
      
          callback();
        });
      },
    ], callback);
  });

  // create dialog
  waterfall.step('4', function(callback) {
    const dialogInstance = new Dialog({ users });
    const instanceError = dialogInstance.validateSync();

    if (instanceError) {
      return callback(instanceError);
    } else {
      dialogInstance.save(callback);
    }
  });

  // build dialog and send response
  waterfall.finish(function(err, dialog) {
    if (err) return response(err);

    response(null, dialog);
  });

  waterfall.add('1');
  waterfall.add('2');
  waterfall.add('3');
  waterfall.add('4');

  waterfall.run();
}