import { pick, isEqual } from 'lodash';

export default ({ Dialog }, { createWaterfall }, { Caesar }) => ({request}, response) => {
  const waterfall = createWaterfall();

  // get contacts list
  waterfall.step('1', function(callback) {
    Caesar.getFullContactsList({ id: request.userId }, function(err, payload) {
      if (err) return callback(err);

      const {contacts} = payload;
      
      if (contacts.length === 0) {
        return callback(null, []);
      }

      waterfall.add('2');

      callback(null, contacts);
    });
  });
  
  // find dialogs for existing contacts
  waterfall.step('2', function(contacts, callback) {
    Dialog
      .aggregate([
        {
          $match: { profiles: request.userId },
        },
        {
          $project: {
            _id: 0,
            id: 1,
            profiles: 1,
          },
        },
      ])
      .exec(function(err, dialogs) {
        if (err) return callback(err);

        waterfall.add('3');

        callback(null, contacts, dialogs);
      });
  });

  // create dialogs for contacts without dialog
  waterfall.step('3', function(contacts, dialogs, callback) {
    if (contacts.length !== dialogs.length) {
      const instances = [];
      
      if (dialogs.length > 0) {
        dialogs.forEach(function(dialog) {
          contacts.forEach(function(contact) {
            if (!isEqual(dialog.profiles, contact.profiles)) {
              instances.push(new Dialog({ profiles: contact.profiles.map(profile => profile.id) }));
            }
          });
        });
      } else {
        contacts.forEach(function(contact) {
          instances.push(new Dialog({ profiles: contact.profiles.map(profile => profile.id) }));
        });
      }

      waterfall.add('3.1');

      callback(null, contacts, instances);
    } else {
      waterfall.add('3.2');

      callback(null, contacts, dialogs);
    }
  });
  
  // save dialogs for contacts without dialog
  waterfall.step('3.1', function(contacts, instances, callback) {
    Dialog
      .insertMany(instances)
      .then(dialogs => callback(null, contacts, dialogs.map(dialog => dialog.toObject())))
      .catch(err => callback(err));
  });

  // get dialogs with last message
  waterfall.step('3.2', function(contacts, dialogs, callback) {
    Dialog
      .aggregate([
        {
          $match: {
            id: {
              $in: dialogs.map(dialog => dialog.id),
            },
          },
        },
        {
          $lookup: {
            from: 'messages',
            localField: 'id',
            foreignField: 'dialogId',
            as: 'messages',
          },
        },
        {
          $unwind: {
            path: '$messages',
            preserveNullAndEmptyArrays: true,
          },
        },
        {
          $sort: {
            'messages.createdAt': 1,
          },
        },
        {
          $group: {
            _id: {
              id: '$id',
              profiles: '$profiles',
              createdAt: '$createdAt',
              updatedAt: '$updatedAt',
            },
            lastMessage: { $last: '$messages' },
            unreadCount: {
              $sum: {
                $cond: [ 
                  {
                    $and: [
                      { $eq: ['$messages.readed', false] },
                      { $ne: ['$messages.profileId', request.userId] },
                    ],
                  },
                  1,
                  0,
                ],
              },
            },
          },
        },
        {
          $project: {
            _id: 0,
            id: '$_id.id',
            profiles: '$_id.profiles',
            lastMessage: {
              id: '$lastMessage.id',
              body: '$lastMessage.body',
              profileId: '$lastMessage.profileId',
              readed: '$lastMessage.readed',
              createdAt: '$lastMessage.createdAt',
              updatedAt: '$lastMessage.updatedAt',
            },
            unreadCount: 1,
            createdAt: '$_id.createdAt',
            updatedAt: '$_id.updatedAt',
          },
        },
      ])
      .exec(function(err, dialogs) {
        if (err) return callback(err);

        callback(null, contacts, dialogs);
      });
  });

  // build response and send
  waterfall.finish(function(err, contacts, dialogs) {
    if (err) return response(err);

    if (dialogs.length > 0) {
      dialogs = dialogs.map(function(dialog) {
        contacts.forEach(function(contact) {
          if (isEqual(dialog.profiles, contact.profiles.map(profile => profile.id))) {
            dialog.profiles = contact.profiles;
          }
        });

        dialog.profile = dialog.profiles.filter(function(profile) {
          return profile.id !== request.userId;
        }).shift();

        return pick(dialog, [
          'id',
          'profile',
          'lastMessage',
          'unreadCount',
        ]);;
      })
    }

    response(null, dialogs);
  });

  waterfall.add('1');

  waterfall.run();
}