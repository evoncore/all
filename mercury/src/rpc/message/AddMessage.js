import { isEmpty, pick } from 'lodash';

export default ({ Dialog, Message }, { createWaterfall }) => ({request}, response) => {
  const waterfall = createWaterfall();

  // check if dialog exists
  waterfall.step('1', function(callback) {
    Dialog.findOne({ id: request.dialogId }, function(err, dialog) {
      if (err) return callback(err);

      if (isEmpty(dialog)) {
        return callback(new Error('You can not send messages to the dialogue which does not exist.'))
      }

      callback(null, dialog);
    });
  });

  // create message and save
  waterfall.step('2', function(dialog, callback) {
    const messageInstance = new Message({
      profileId: request.userId,
      dialogId: dialog.id,
      body: request.body,
    });

    const instanceError = messageInstance.validateSync();

    if (instanceError) {
      return callback(null, instanceError);
    }

    messageInstance.save(function(err, message) {
      if (err) return callback(err);

      callback(null, {
        message: pick(message, [
          'id',
          'body',
          'profileId',
          'createdAt',
          'updatedAt',
        ]),
        socketMessage: {
          from: message.profileId,
          to: dialog.profiles.filter(id => id !== request.userId).shift(),
        },
      });
    });
  });

  // build response and send
  waterfall.finish(function(err, payload) {
    if (err) return response(err);

    response(null, payload);
  });

  waterfall.add('1');
  waterfall.add('2');

  waterfall.run();
}