import { isEmpty, pick } from 'lodash';

export default ({ Dialog, Message }, { createWaterfall }) => ({request}, response) => {
  const waterfall = createWaterfall();

  waterfall.step('1', function(callback) {
    Dialog
      .findOne({ id: request.dialogId }, function(err, dialog) {
        if (err) return callback(err);

        if (isEmpty(dialog)) {
          return callback(new Error('You can not receive messages from the dialogue which does not exist.'));
        }

        callback(null, dialog);
      });
  });

  waterfall.finish(function(err, dialog) {
    if (err) return response(err);

    Message.find({ dialogId: dialog.id }, function(err, messages) {
      if (err) return response(err);

      response(null, messages.map(message => pick(message, [
        'id',
        'body',
        'profileId',
        'createdAt',
        'updatedAt',
      ])));
    });
  });

  waterfall.add('1');

  waterfall.run();
}