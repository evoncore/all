export default ({ Message }) => ({request}, response) => {
  Message
    .updateMany({
      dialogId: request.dialogId,
      profileId: { $ne: request.userId },
    }, {
      readed: true,
    })
    .exec(function(err) {
      if (err) return response(err);

      response();
    });
}