import AWS from 'aws-sdk';

require('../../framework').service({
  bindIntoMethods: {
    s3: new AWS.S3(),
    helpers: require('./helpers'),
  },
});