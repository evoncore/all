import { takeRight } from 'lodash';

export function parseUrl(url) {
  return {
    Bucket: url.split('//').pop().split('.').shift(),
    Key: takeRight(url.split('/')).shift(),
  };
}