export default () => function({request}, response) {
  const params = this.helpers.parseUrl(request.url);

  this.s3.deleteObject(params, function(err) {
    if (err) return response(err);

    response();
  });
}