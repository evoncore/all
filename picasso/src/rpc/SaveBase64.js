export default ({}, { config, uuid }) => function({request}, response) {
  const {base64} = request;
  const type = base64.split(';')[0].split('/')[1];
  const params = {
    Bucket: config.AWS.Bucket,
    Body: new Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ''), 'base64'),
    Key: uuid(),
    ContentEncoding: 'base64',
    ContentType: `image/${type}`,
  };

  if (!request.private) {
    params.ACL = 'public-read';
  }

  this.s3.upload(params, function (err, data) {
    if (err) return response(err);

    response(null, {
      bucket: data.Bucket,
      key: data.Key,
      url: data.Location,
    });
  });
}