import { merge } from 'lodash';

export default () => function({request}, response) {
  const {base64} = request;
  const type = base64.split(';')[0].split('/')[1];
  const params = {
    Body: new Buffer.from(base64.replace(/^data:image\/\w+;base64,/, ''), 'base64'),
    ContentEncoding: 'base64',
    ContentType: `image/${type}`,
  };

  merge(params, this.helpers.parseUrl(request.url));

  if (!request.private) {
    params.ACL = 'public-read';
  }

  this.s3.upload(params, function (err, data) {
    if (err) return response(err);

    response();
  });
}