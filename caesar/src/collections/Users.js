export default function({ mongoose, connection }, { uuid, unix }) {
  const schema = new mongoose.Schema({
    id: {
      type: String,
      required: true,
      default: uuid,
    },
    profile: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Profile',
      required: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
    },
    phoneNumber: {
      type: String,
      required: true,
      unique: true,
    },
    password: {
      type: String,
      required: true,
    },
    createdAt: {
      type: Number,
      required: true,
      default: unix,
    },
    updatedAt: {
      type: Number,
      required: true,
      default: unix,
    },
  });

  return connection.model('User', schema);
}