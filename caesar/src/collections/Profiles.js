export default function({ connection, mongoose }, { unix, uuid }) {
  const schema = new mongoose.Schema({
    id: {
      type: String,
      required: true,
      default: uuid,
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User',
      required: true,
    },
    firstName: {
      type: String,
      required: true,
    },
    lastName: {
      type: String,
      required: true,
    },
    birthday: {
      type: Number,
      required: true,
    },
    country: {
      type: String,
      required: true,
    },
    gender: {
      type: Number,
      enum: [0, 1],
    },
    professionalPosition: {
      type: String,
      required: true
    },
    avatarUrl: {
      type: String,
    },
    createdAt: {
      type: Number,
      required: true,
      default: unix,
    },
    updatedAt: {
      type: Number,
      required: true,
      default: unix,
    },
  });

  try {
    schema.index({
      firstName: 'text',
      lastName: 'text',
      professionalPosition: 'text',
    }, {
      weights: {
        firstName: 2,
        lastName: 1,
        professionalPosition: 3,
      },
    });
  } catch (exception) {
    console.info(exception);
  }

  return connection.model('Profile', schema);
}