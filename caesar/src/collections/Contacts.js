const ContactStatus = {
  Requested: 'Requested',
  Created: 'Created',
  Declined: 'Declined',
};

const contactStatuses = Object.values(ContactStatus);

export default function({ mongoose, connection }, { unix, uuid }) {
  const schema = new mongoose.Schema({
    id: {
      type: String,
      required: true,
      default: uuid,
    },
    profileId: {
      type: String,
      required: true,
    },
    requestedProfileId: {
      type: String,
      required: true,
    },
    status: {
      type: String,
      enum: contactStatuses,
      default: contactStatuses[0],
    },
    profiles: [{
      type: String,
    }],
    createdAt: {
      type: Number,
      required: true,
      default: unix,
    },
    updatedAt: {
      type: Number,
      required: true,
      default: unix,
    },
  });

  const model = connection.model('Contact', schema);

  model.Status = ContactStatus;

  return model;
}