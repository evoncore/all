export default ({ Profiles }) => ({request}, response) => {
  Profiles
    .aggregate([
      {
        $match: {
          $text: {
            $search: request.query,
          },
        },
      },
      {
        $sort: {
          score: {
            $meta: 'textScore',
          },
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'user',
        },
      },
      {
        $group: {
          _id: {
            id: '$id',
            firstName: '$firstName',
            lastName: '$lastName',
            birthday: '$birthday',
            country: '$country',
            gender: '$gender',
            professionalPosition: '$professionalPosition',
            user: {
              $arrayElemAt: ['$user', 0],
            },
          }
        },
      },
      {
        $project: {
          _id: 0,
          id: '$_id.id',
          firstName: '$_id.firstName',
          lastName: '$_id.lastName',
          birthday: '$_id.birthday',
          country: '$_id.country',
          gender: '$_id.gender',
          professionalPosition: '$_id.professionalPosition',
          email: '$_id.user.email',
          phoneNumber: '$_id.user.phoneNumber',
        },
      },
    ])
    .exec(function(err, data) {
      if (err) return response(err);

      response(null, { data });
    });
};