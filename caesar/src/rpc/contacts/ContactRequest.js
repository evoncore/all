import { isEmpty, pick } from 'lodash';

export default (
  { Profiles, Contacts }, 
  { unix, createWaterfall },
) => ({request}, response) => {
  const waterfall = createWaterfall();

  // validate request
  waterfall.step('1', function(callback) {
    if (request.id === request.requestedId) {
      return callback(new Error('You can’t request a contact to yourself.'));
    }

    callback();
  });

  // find for existing contact connection
  waterfall.step('2', function(callback) {
    Contacts.findOne(
      {
        $or: [
          {
            profileId: request.id,
            requestedProfileId: request.requestedId,
          },
          {
            profileId: request.requestedId,
            requestedProfileId: request.id,
          },
        ],
      },
      (err, contact) => {
        if (err) return callback(err);

        if (!isEmpty(contact)) {
          switch (contact.status) {
            case Contacts.Status.Requested: return callback(new Error('Request for contact already exists.'));
            case Contacts.Status.Created: return callback(new Error('User already in your contact list.'));
          }

          // skip steps 3, 4 and 5
          if (contact.status === Contacts.Status.Declined) {
            waterfall.add('2.1');
            return callback(null, contact);
          }
        }

        waterfall.add('3');
        waterfall.add('4');
        waterfall.add('5');

        callback();
      });
  });

  // updated status "Declined" contact connection to "Requested"
  waterfall.step('2.1', function(contact, callback) {
    contact.update({ status: Contacts.Status.Requested, updatedAt: unix() }, (err) => {
      if (err) return callback(err);

      contact.status = Contacts.Status.Requested;

      callback(null, contact);
    });
  });

  // find profile who request contact connection
  waterfall.step('3', function(callback) {
    Profiles
      .findOne({ id: request.id })
      .exec(function(err, owner) {
        if (err) return callback(err);
  
        if (isEmpty(owner)) {
          return callback(new Error('User not found.'));
        }

        callback(null, owner);
      });
  });

  // find profile who requested for contact connection
  waterfall.step('4', function(owner, callback) {
    Profiles
      .findOne({ id: request.requestedId })
      .exec(function(err, requestedProfile) {
        if (err) {
          return callback(err);
        }

        if (isEmpty(requestedProfile)) {
          return callback(new Error('Requested user not found.'));
        }

        callback(null, owner, requestedProfile);
      });
  });

  // create contact with status "Requested"
  waterfall.step('5', function(owner, requestedProfile, callback) {
    const contactInstance = new Contacts({
      profileId: owner.id,
      requestedProfileId: requestedProfile.id,
    });
    
    const instanceError = contactInstance.validateSync();

    if (instanceError) {
      return callback(instanceError);
    }

    contactInstance.save(function(err, contact) {
      if (err) return callback(err);

      callback(null, contact);
    });
  });

  // build created contact and send response
  waterfall.finish(function(err, contact) {
    if (err) return response(err);

    contact = contact.toObject();
    contact.socketMessage = {
      from: contact.profileId,
      to: contact.requestedProfileId,
    };
  
    contact = pick(contact, [
      'id',
      'createdAt',
      'updatedAt',
      'socketMessage',
    ]);

    response(null, contact);
  });

  // the beginning of the waterfall from the first two steps - necessarily
  waterfall.add('1');
  waterfall.add('2');

  waterfall.run();
};