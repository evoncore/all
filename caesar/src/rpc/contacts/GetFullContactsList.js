export default ({ Contacts }) => ({request}, response) => {
  Contacts
    .aggregate([
      {
        $match: {
          $and: [
            { status: Contacts.Status.Created },
            {
              $or: [
                { profileId: request.id },
                { requestedProfileId: request.id },
              ],
            },
          ]
        },
      },
      {
        $lookup: {
          from: 'profiles',
          localField: 'profiles',
          foreignField: 'id',
          as: 'profiles',
        },
      },
      {
        $project: {
          _id: 0,
          id: 1,
          status: 1,
          profiles: {
            $map: {
              input: '$profiles',
              as: 'profile',
              in: {
                id: '$$profile.id',
                firstName: '$$profile.firstName',
                lastName: '$$profile.lastName',
                professionalPosition: '$$profile.professionalPosition',
              },
            },
          },
          createdAt: 1,
          updatedAt: 1,
        },
      },
    ])
    .exec((err, contacts) => {
      if (err) return response(err);

      response(null, contacts);
    });
};