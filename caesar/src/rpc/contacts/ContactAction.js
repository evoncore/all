import { isEmpty } from 'lodash';
import getContactsList from './getContactsList';

export default (models, utils) => ({request}, response) => {
  const { Contacts } = models;
  const { unix, createWaterfall } = utils;
  const { accepted, userId } = request;
  const waterfall = createWaterfall();

  // find contact for change status
  waterfall.step('1', function(callback) {
    Contacts
      .findOne({ id: request.contactId })
      .exec(function(err, contact) {
        if (err) return callback(err);
  
        if (isEmpty(contact)) {
          return callback(new Error('Contact not found.'));
        }

        callback(null, contact);
      });
  });

  // change contact status, build changed contact and send response
  waterfall.finish(function(err, contact) {
    if (err) return response(err);

    const status = accepted ? Contacts.Status.Created : Contacts.Status.Declined;
    const profiles = accepted ?  [contact.profileId, contact.requestedProfileId] : [];

    contact.update({ status, profiles, updatedAt: unix() }, function(err) {
      if (err) return response(err);

      getContactsList(models, utils)({ request: { id: userId } }, (err, contacts) => {
        if (err) return response(err);

        response(null, {
          contacts,
          accepted,
          socketMessage: {
            from: contact.requestedProfileId,
            to: contact.profileId,
          },
        });
      });
    });
  });

  waterfall.add('1');
  
  waterfall.run();
};