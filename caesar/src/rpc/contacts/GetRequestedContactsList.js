export default ({ Contacts }) => ({request}, response) => {
  Contacts
    .aggregate([
      {
        $match: {
          requestedProfileId: request.id,
          status: Contacts.Status.Requested,
        },
      },
      {
        $lookup: {
          from: 'profiles',
          localField: 'profileId',
          foreignField: 'id',
          as: 'profile',
        },
      },
      {
        $group: {
          _id: {
            id: "$id",
            status: "$status",
            profile: {
              $arrayElemAt: ["$profile", 0],
            },
            createdAt: "$createdAt",
            updatedAt: "$updatedAt",
          },
        },
      },
      {
        $project: {
          _id: 0,
          id: "$_id.id",
          status: "$_id.status",
          profile: {
            id: "$_id.profile.id",
            firstName: "$_id.profile.firstName",
            lastName: "$_id.profile.lastName",
            professionalPosition: "$_id.profile.professionalPosition",
          },
          createdAt: "$_id.createdAt",
          updatedAt: "$_id.updatedAt",
        },
      },
    ])
    .exec((err, requestedContacts) => {
      if (err) return response(err);

      response(null, requestedContacts);
    });
};