export default ({ Contacts }) => ({request}, response) => {
  Contacts
    .aggregate([
      {
        $match: {
          $and: [
            { status: Contacts.Status.Created },
            {
              $or: [
                { profileId: request.id },
                { requestedProfileId: request.id },
              ],
            },
          ]
        },
      },
      {
        $lookup: {
          from: 'profiles',
          localField: 'profiles',
          foreignField: 'id',
          as: 'profiles',
        },
      },
      {
        $group: {
          _id: {
            id: "$id",
            status: "$status",
            profile: {
              $arrayElemAt: [{
                $filter: {
                  input: '$profiles',
                  as: 'profile',
                  cond: {
                    $ne: ['$$profile.id', request.id],
                  },
                },
              }, 0],
            },
            createdAt: "$createdAt",
            updatedAt: "$updatedAt",
          },
        },
      },
      {
        $project: {
          _id: 0,
          id: "$_id.id",
          status: "$_id.status",
          profile: {
            id: "$_id.profile.id",
            firstName: "$_id.profile.firstName",
            lastName: "$_id.profile.lastName",
            professionalPosition: "$_id.profile.professionalPosition",
          },
          createdAt: "$_id.createdAt",
          updatedAt: "$_id.updatedAt",
        },
      },
    ])
    .exec((err, contacts) => {
      if (err) return response(err);

      response(null, contacts);
    });
};