import { isEmpty } from 'lodash';

export default (
  { Profiles }, 
  { createWaterfall }, 
  { Picasso },
) => ({request}, response) => {
  const waterfall = createWaterfall();

  waterfall.step('1', function(callback) {
    Profiles.findOne({ id: request.id }, function(err, profile) {
      if (err) return callback(err);

      if (isEmpty(profile)) {
        return callback(new Error('User not found.'));
      }

      callback(null, profile);
    });
  });

  waterfall.step('2', function(profile, callback) {
    if (isEmpty(profile.avatarUrl)) {
      callback(new Error('Avatar does not exists.'));
    } else {
      Picasso.removeBase64({ url: profile.avatarUrl }, function(err) {
        if (err) return callback(err);

        callback(null, profile);
      });
    }
  });

  waterfall.step('3', function(profile, callback) {
    profile.update({ $unset: { avatarUrl: null } }, function(err) {
      if (err) return callback(err);

      callback();
    });
  });

  waterfall.finish(function(err) {
    if (err) return response(err);

    response();
  });

  waterfall.add('1');
  waterfall.add('2');
  waterfall.add('3');

  waterfall.run();
}