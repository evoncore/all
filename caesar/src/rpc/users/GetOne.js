import { isEmpty } from 'lodash';

export default ({ Profiles }) => ({request}, response) => {
  Profiles
    .aggregate([
      {
        $match: {
          id: request.id,
        },
      },
      {
        $lookup: {
          from: 'users',
          localField: 'user',
          foreignField: '_id',
          as: 'user',
        },
      },
      {
        $group: {
          _id: {
            id: '$id',
            firstName: '$firstName',
            lastName: '$lastName',
            birthday: '$birthday',
            country: '$country',
            gender: '$gender',
            professionalPosition: '$professionalPosition',
            avatarUrl: '$avatarUrl',
            user: {
              $arrayElemAt: ['$user', 0],
            },
          },
        },
      },
      {
        $project: {
          _id: 0,
          id: '$_id.id',
          firstName: '$_id.firstName',
          lastName: '$_id.lastName',
          birthday: '$_id.birthday',
          country: '$_id.country',
          gender: '$_id.gender',
          professionalPosition: '$_id.professionalPosition',
          email: '$_id.user.email',
          phoneNumber: '$_id.user.phoneNumber',
          avatarUrl: '$_id.avatarUrl',
        },
      },
    ])
    .exec(function(err, [profile]) {
      if (err) return response(err);

      if (isEmpty(profile)) {
        return response(new Error('User not found.'));
      }

      response(null, profile);
    });
};