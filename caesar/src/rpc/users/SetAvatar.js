import { isEmpty } from 'lodash';

export default (
  { Profiles }, 
  { createWaterfall }, 
  { Picasso },
) => ({request}, response) => {
  const waterfall = createWaterfall();

  waterfall.step('1', function(callback) {
    if (isEmpty(request.base64)) {
      return callback(new Error('base64 is required.'));
    }

    callback();
  });

  waterfall.step('2', function(callback) {
    Profiles.findOne({ id: request.id }, function(err, profile) {
      if (err) return callback(err);

      if (isEmpty(profile)) {
        return callback(new Error('User not found.'));
      }

      callback(null, profile);
    });
  });

  waterfall.step('3', function(profile, callback) {
    const params = {
      base64: request.base64,
      private: false,
    };

    if (!isEmpty(profile.avatarUrl)) {
      params.url = profile.avatarUrl;

      Picasso.updateBase64(params, function(err) {
        if (err) return callback(err);

        callback(null, profile.avatarUrl);
      });
    } else {
      Picasso.saveBase64(params, function(err, payload) {
        if (err) return callback(err);

        waterfall.add('4');
  
        callback(null, profile, payload.url);
      });
    }
  });

  waterfall.step('4', function(profile, avatarUrl, callback) {
    profile.update({ avatarUrl }, function(err) {
      if (err) return callback(err);

      callback(null, avatarUrl);
    });
  });

  waterfall.finish(function(err, avatarUrl) {
    if (err) return response(err);

    response(null, avatarUrl);
  });

  waterfall.add('1');
  waterfall.add('2');
  waterfall.add('3');

  waterfall.run();
}