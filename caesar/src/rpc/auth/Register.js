import { pick } from 'lodash';

export default (
  { Users, Profiles },
  { createWaterfall, ObjectId },
) => ({request}, response) => {
  const waterfall = createWaterfall();
  const userId = ObjectId();
  const profileId = ObjectId();

  const userData = pick(request.user, [
    'email',
    'phoneNumber',
    'password',
  ]);

  const profileData = pick(request.profile, [
    'firstName',
    'lastName',
    'birthday',
    'country',
    'gender',
    'professionalPosition',
  ]);

  userData._id = userId;
  profileData._id = profileId;

  userData.profile = profileId;
  profileData.user = userId;

  // prepare user instance for creating
  waterfall.step('1', function(callback) {
    const userInstance = new Users(userData);

    callback(userInstance.validateSync(), userInstance);
  });

  // create profile for user
  waterfall.step('2', function(userInstance, callback) {
    const profileInstance = new Profiles(profileData);

    profileInstance.save(function(err, profile) {
      if (err) return callback(err);

      callback(null, userInstance, profile);
    });
  });

  // save user instance with profile and send response
  waterfall.finish(function(err, userInstance, profile) {
    if (err) return callback(err);

    userInstance.save(function(err, user) {
      if (err) return response(err);

      profile = pick(profile.toObject(), [
        'id',
        'firstName',
        'lastName',
        'birthday',
        'country',
        'gender',
        'professionalPosition',
      ]);

      profile.email = user.email;
      profile.phoneNumber = user.phoneNumber;

      response(null, {
        token: 'Some token',
        profile,
      });
    });
  })

  waterfall.add('1');
  waterfall.add('2');

  waterfall.run();
};