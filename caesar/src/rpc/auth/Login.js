import { isEmpty, omit } from 'lodash';

const emailPattern = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
const phoneNumberPattern = /\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/;

export default ({ Users }) => ({request}, response) => {
  const { login, password } = request;

  const isEmail = () => emailPattern.test(login);
  const isPhoneNumber = () => phoneNumberPattern.test(login);

  let field = 'email';

  if (isEmail()) {
    field = 'email';
  } else if (isPhoneNumber()) {
    field = 'phoneNumber';
  }

  Users
    .aggregate([
      {
        $match: {
          [field]: login,
        },
      },
      {
        $lookup: {
          from: 'profiles',
          localField: 'profile',
          foreignField: '_id',
          as: 'profile',
        },
      },
      {
        $group: {
          _id: {
            id: '$id',
            email: '$email',
            phoneNumber: '$phoneNumber',
            password: '$password',
            profile: {
              $arrayElemAt: ['$profile', 0],
            },
          }
        },
      },
      {
        $project: {
          _id: 0,
          id: '$_id.profile.id',
          firstName: '$_id.profile.firstName',
          lastName: '$_id.profile.lastName',
          birthday: '$_id.profile.birthday',
          country: '$_id.profile.country',
          gender: '$_id.profile.gender',
          professionalPosition: '$_id.profile.professionalPosition',
          email: "$_id.email",
          phoneNumber: '$_id.phoneNumber',
          password: '$_id.password',
        },
      },
    ])
    .exec((err, [profile]) => {
      if (err) return response(err);

      if (isEmpty(profile) || password !== profile.password) {
        response(new Error('Invalid login or password.'));
      }

      response(null, {
        token: 'some token',
        profile: omit(profile, 'password'),
      });
    });
};