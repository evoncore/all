export default function({ mongoose, connection }, { uuid, unix }) {
  const schema = new mongoose.Schema({
    id: {
      type: String,
      required: true,
      default: uuid,
    },
    createdAt: {
      type: Number,
      required: true,
      default: unix,
    },
    updatedAt: {
      type: Number,
      required: true,
      default: unix,
    },
  });

  return connection.model('ExampleName', schema);
}