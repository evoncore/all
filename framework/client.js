import fs from 'fs';
import grpc from 'grpc';
import { capitalize, isEmpty, takeRight } from 'lodash';

export function client(name) {
  const mapURI = `${__dirname}/map.json`;
  const proto = grpc.load(`../proto/${name}.proto`);
  const config = require('./config.json');
  let map;

  try {
    map = require(mapURI);
  } catch (exception) {
    map = {};
  }

  let service = map[name];
  const keys = Object.keys(map);

  if (isEmpty(service)) {
    const lastKey = takeRight(keys);
    const newService = { 
      ip: config.InitialIP, 
      port: config.InitialPort,
    };

    if (keys.length > 0) {
      const lastService = map[lastKey];

      newService.ip = config.InitialIP;
      newService.port = (lastService.port + 1);
    }
    
    map[name] = newService;
    service = newService;
    fs.writeFileSync(mapURI, JSON.stringify(map), 'utf8');
  }

  return new proto[name][`${capitalize(name)}Service`](`${service.ip}:${service.port}`, grpc.credentials.createInsecure());
}