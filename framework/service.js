import fs from 'fs';
import grpc from 'grpc';
import mongoose from 'mongoose';
import { isFunction } from 'lodash';
import { config } from './helpers';
import { usedir } from './usedir';
import { isEmpty, forEach, takeRight, capitalize } from 'lodash';
import { client } from './client';

const mapURI = `${__dirname}/map.json`;

export function service(options = {}) {
  const frameworkConfig = require('./config');
  const { onStart, bindIntoMethods } = options;
  const { FilePath, PackageName, ServiceName } = config.Proto;
  const server = new grpc.Server();
  const app = {};

  app.withDatabase = !isEmpty(config.Database);

  if (app.withDatabase) {
    app.mongoose = mongoose;
    app.connection = mongoose.createConnection(config.Database.URI);
  }

  app.utils = usedir({ 
    cwd: __dirname, 
    dirName: 'utils', 
    execute: false,
  });

  app.utils.config = config;

  if (app.withDatabase) {
    app.utils.ObjectId = mongoose.Types.ObjectId;

    app.collections = usedir({ 
      dirName: 'src/collections', 
      execute: true, 
      args: [{ mongoose: app.mongoose, connection: app.connection }, app.utils], 
    });
  }
  
  app.clients = {};
  
  (config.Clients || []).map(name => app.clients[capitalize(name)] = client(name));

  app.methods = usedir({ 
    dirName: 'src/rpc',
    execute: true,
    args: app.withDatabase ? [app.collections, app.utils, app.clients] : [{}, app.utils, app.clients],
    transformFileNameCaseTo: 'camel',
  });

  if (!isEmpty(bindIntoMethods)) {
    forEach(app.methods, (method, key) => {
      app.methods[key] = method.bind(bindIntoMethods);
    });
  }

  server.addProtoService(
    grpc.load(FilePath)[PackageName][ServiceName]['service'], app.methods
  );

  server.bind(
    registerService(frameworkConfig, PackageName), 
    grpc.ServerCredentials.createInsecure()
  );

  server.start();

  if (isFunction(onStart)) {
    const params = {
      server,
      clients: app.clients,
      methods: app.methods,
      utils: app.utils,
      config,
    };

    if (app.withDatabase) {
      params.mongoose = app.mongoose;
      params.connection = app.connection;
      params.collections = app.collections;
    }
    
    onStart(params);
  }
}

function registerService(frameworkConfig, name) {
  const host = {
    ip: frameworkConfig.InitialIP,
    port: frameworkConfig.InitialPort,
  };

  let map;

  try {
    map = require(mapURI);
  } catch (exception) {
    map = {};
  }

  const keys = Object.keys(map);

  if (keys.length > 0) {
    const service = map[name];

    if (isEmpty(service)) {
      const lastKey = takeRight(keys);

      host.ip = frameworkConfig.InitialIP;
      host.port = (map[lastKey].port + 1);
    } else {
      host.ip = service.ip;
      host.port = service.port;
    }
  }

  map[name] = host;

  fs.writeFileSync(mapURI, JSON.stringify(map), 'utf8');
  
  log(host);

  return `${host.ip}:${host.port}`;
}

function log(host) {
  console.log(
    JSON
      .stringify(host)
      .replace('{', '')
      .replace('}', '')
      .replace(',', ' ')
      .replace(new RegExp('"', 'g'), '')
      .replace(new RegExp(':', 'g'), ': ')
  );
}