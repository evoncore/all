import fs from 'fs';
import path from 'path';
import _, { isObject, isEmpty } from 'lodash';

export function usedir(options) {
  let savedPath, savedObject;

  if (isObject(this) && !isEmpty(this)) {
    savedPath = this.savedPath;
    savedObject = this.savedObject;
  }

  const { cwd, execute, dirName, args, transformFileNameCaseTo } = options;
  const pathToDir = savedPath || path.resolve(cwd || process.cwd(), dirName);
  const object = savedObject || {};

  fs.readdirSync(pathToDir).forEach((fileName) => {
    if (fileName.startsWith('.') || fileName.startsWith('index')) return; // skip hidden files, directories and index files

    const fullPath = `${pathToDir}/${fileName}`;

    if (fs.lstatSync(fullPath).isDirectory()) {
      usedir.call({ savedPath: fullPath, savedObject: object }, options);
    } else {
      let name = getName(fileName);

      if (transformFileNameCaseTo) {
        try {
          name = _[`${transformFileNameCaseTo}Case`](name);
        } catch (exception) {
          console.info(exception);
        }
      }

      if (execute) {
        object[name] = getModule(fullPath)(...args);
      } else {
        object[name] = getModule(fullPath);
      }
    }
  });

  return object;
}

function getName(fileName) {
  return fileName.split('.').shift();
}

function getModule(fullPath) {
  try {
    return require(fullPath).default;
  } catch (exception) {
    try {
      return require(fullPath);
    } catch (exception) {
      throw new Error(exception);
    }
  }
}