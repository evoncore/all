import namespace from 'uuid/v3';
import random from 'uuid/v4';
import { config } from '../helpers';

export default function() {
  return namespace(random(), config.App.UUID);
}
