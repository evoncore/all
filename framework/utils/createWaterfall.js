import { waterfall } from 'async';

export default function() {
  const steps = {};
  const stack = [];

  let initFunc;
  let finishFunc = function() {};

  return {
    init: function(func) {
      initFunc = func;
    },
    step: function(id, func) {
      steps[id] = func;
    },
    add: function(funcId) {
      stack.push(steps[funcId]);
    },
    finish: function(func) {
      finishFunc = func;
    },
    run: function() {
      if (initFunc) {
        stack.unshift(init);
      }

      waterfall(stack, finishFunc);
    },
  };
}