import moment from 'moment';

export default function(value = new Date()) {
  return moment(value).unix();
}