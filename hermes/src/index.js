import express from 'express';
import http from 'http';
import socketIO from 'socket.io';
import bodyParser from 'body-parser';
import config from '../config.json';
import endpoints from './endpoints';
import sockets from './sockets';
import events from './events';
import { EventEmitter } from 'events';

const emitter = new EventEmitter();
const app = express();
const server = http.Server(app);
const io = socketIO(server);

app.use(bodyParser.json({ limit: config.JsonLimit }));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type");
  next();
});

endpoints(app, emitter, 'v1');
sockets(io);
events(emitter, io);

app.use(function(req, res) {
  res.status(404).send({ message: 'Not Found', code: 404 });
});

server.listen(config.Port, config.IP, function() {
  console.log('Host:', `${config.IP}:${config.Port}`);
});

io.on('connection', sockets);
