export default function(emitter, io) {

  emitter.on('contactRequest', function(socketMessage) {
    io.to(socketMessage.to).emit('contactRequest', socketMessage.from);
  });

  emitter.on('contactAction', function({ socketMessage, accepted }) {
    io.to(socketMessage.to).emit('contactAction', {
      id: socketMessage.from,
      accepted,
    });
  });

  emitter.on('message', function({ socketMessage, message }) {
    io.to(socketMessage.to).emit('message', message);
  });

}