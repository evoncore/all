export default function(io) {
  io.on('connection', function(socket) {
    const userId = socket.handshake.query.userId;

    socket.join(userId);

    socket.on('disconnect', function() {
      socket.leave(userId);
    });
  });
}