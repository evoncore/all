import { Method } from '../../constaints';

export default [
  {
    method: Method.POST,
    path: '/account/login',
    call: 'login',
    data: {
      login: 'body.login',
      password: 'body.password',
    },
  },
  {
    method: Method.POST,
    path: '/account/register',
    call: 'register',
    data: {
      user: 'body.user',
      profile: 'body.profile',
    },
  },
  {
    method: Method.GET,
    path: '/user/:id',
    call: 'getOne',
    data: {
      id: 'params.id',
    },
  },
  {
    method: Method.POST,
    path: '/user/contact',
    call: 'contactRequest',
    data: {
      id: 'body.id',
      requestedId: 'body.requestedId',
    },
  },
  {
    method: Method.POST,
    path: '/user/contact/:id/accept/:userId',
    call: 'contactAction',
    data: {
      userId: 'params.userId',
      contactId: 'params.id',
    },
    props: {
      accepted: true,
    },
    emit: {
      eventName: 'contactAction',
      data: {
        socketMessage: 'socketMessage',
        accepted: 'accepted',
      },
    },
  },
  {
    method: Method.POST,
    path: '/user/contact/:id/decline/:userId',
    call: 'contactAction',
    data: {
      userId: 'params.userId',
      contactId: 'params.id',
    },
    props: {
      accepted: false,
    },
    emit: {
      eventName: 'contactAction',
      data: {
        socketMessage: 'socketMessage',
        accepted: 'accepted',
      },
    },
  },
  {
    method: Method.GET,
    path: '/user/contacts/:id',
    call: 'getContactsList',
    data: { 
      id: 'params.id',
    },
  },
  {
    method: Method.GET,
    path: '/user/contacts/requested/:id',
    call: 'getRequestedContactsList',
    data: { 
      id: 'params.id',
    },
  },
  {
    method: Method.POST,
    path: '/user/:id/avatar',
    call: 'setAvatar',
    data: { 
      id: 'params.id', 
      base64: 'body.base64',
    },
  },
  {
    method: Method.DELETE,
    path: '/user/:id/avatar',
    call: 'removeAvatar',
    data: { 
      id: 'params.id',
    },
  },
];