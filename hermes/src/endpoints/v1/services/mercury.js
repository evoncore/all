import { Method } from '../../constaints';

export default [
  {
    method: Method.GET,
    path: '/user/dialogs/:userId',
    call: 'fetchDialogs',
    data: { 
      userId: 'params.userId',
    },
  },
  {
    method: Method.GET,
    path: '/user/dialogs/:userId/:dialogId/messages',
    call: 'fetchMessages',
    data: { 
      userId: 'params.userId', 
      dialogId: 'params.dialogId',
    },
  },
  {
    method: Method.GET,
    path: '/user/dialogs/:userId/:dialogId/messages/read',
    call: 'readMessages',
    data: { 
      userId: 'params.userId', 
      dialogId: 'params.dialogId',
    },
  },
  {
    method: Method.POST,
    path: '/user/dialogs/:userId/:dialogId/messages/add',
    call: 'addMessage',
    data: {
      userId: 'params.userId', 
      dialogId: 'params.dialogId', 
      body: 'body.body',
    },
    emit: {
      eventName: 'message',
      data: {
        socketMessage: 'socketMessage',
        message: 'message',
      },
    },
  }
];