import { Service, Execute } from '../constaints';

export default {
  [Service.Caesar]: require('./services/caesar').default,
  [Service.Mercury]: require('./services/mercury').default,
  [Execute]: require('./execute').default,
};