import { Caesar } from '../../services';
import { Method } from '../constaints'
import { parallel } from 'async';

export default [
  {
    method: Method.GET,
    path: '/search',
    middlewares: [
      function(req, res) {
        parallel({
          profiles: function(callback) {
            Caesar.search({ query: req.query.value }, (error, payload) => {
              if (error) return;
      
              callback(null, payload.data);
            });
          },
        }, function(error, searchResult) {
          if (error) return res.status(500).send({ message: error.details });

          res.send(searchResult);
        });
      },
    ],
  },
];