export const Method = {
  GET: 'get',
  POST: 'post',
  PUT: 'put',
  DELETE: 'delete',
};

export const Execute = 'execute';

export const Service = {
  Caesar: 'caesar',
  Mercury: 'mercury',
};