import express from 'express';
import { omit, get, merge, isEmpty, forEach, capitalize } from 'lodash';
import { Execute } from './constaints';

export default function(app, emitter, version) {
  const cwd = process.cwd();
  const pathToEndpoints = `${cwd}/src/endpoints/${version}/config`;
  const endpoints = require(pathToEndpoints).default;

  Object.keys(endpoints).forEach(function(serviceName) {
    const router = express.Router();
    const routes = endpoints[serviceName];
  
    if (serviceName === Execute) {
      routes.forEach(function(route) {
        router[route.method.toLowerCase()](route.path, ...route.middlewares);
      });
  
      app.use(`/${version}`, router);
  
      return;
    }
  
    const pathToService = `${cwd}/src/services`;
    const service = require(pathToService)[capitalize(serviceName)];

    routes.forEach(function(route) {
      router[route.method.toLowerCase()](route.path, function(req, res) {
        const data = buildDataObject(route.data, req);
        const props = (route.props || {});

        merge(data, props);

        service[route.call](data, function(error, payload) {
          if (error) {
            res.status(500).send({ message: error.details });
            return;
          }
  
          if (route.emit) {
            emitter.emit(
              route.emit.eventName, 
              buildDataObject(route.emit.data, payload),
            );
          }
  
          res.send(omit(payload, 'socketMessage'));
        });
      });
    });
    
    app.use(`/${version}`, router);
  });
}

function buildDataObject(object, target) {
  const data = {};

  if (isEmpty(object)) {
    return data;
  }

  forEach(object, function(path, key) {
    data[key] = get(target, path);
  });

  return data;
}